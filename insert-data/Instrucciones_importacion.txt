PASOS PARA IMPORTAR LOS DATOS INICIALES A LA DB.

Debe importarse en el siguiente orden a fin de no romper las relaciones entre tablas

1) insert-branch.json
2) insert-country.json
3) insert-city.json
4) insert-client.json
5) insert-account.json
6) insert-cards.json
7) insert-transactions.json
8) insert-invesments.json
