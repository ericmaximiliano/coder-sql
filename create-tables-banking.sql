create schema `banking`;

use banking;

create table branch_office(
id_branch int not null auto_increment primary key,
name varchar(20) not null,
address varchar(30) not null,
city int not null,
country int not null 
);

create table country(
id_country int not null auto_increment primary key,
name varchar(30) not null,
region varchar(10)
);

create table city(
id_city int not null auto_increment primary key,
name varchar(30) not null,
id_country int not null,
foreign key (id_country) references country(id_country)
);

create table clients(
id_client int not null unique primary key,
name varchar(30) not null,
last_name varchar(30) not null,
address varchar(50) not null,
city int not null,
country int not null,
e_mail varchar(50),
date_birth date not null,
date_start date not null,
date_end date not null,
assigned_branch int,
foreign key (city) references city(id_city),
foreign key (country) references country(id_country),
foreign key (assigned_branch) references branch_office(id_branch)
);

create table accounts(
id_account int not null unique primary key,
client int not null,
type varchar(02) not null,
currency varchar(03) not null,
cbu varchar(22) not null,
alias varchar(20) not null,
balance numeric(12,2) not null,
foreign key (client) references clients(id_client)
);

create table cards(
id_card int not null unique primary key,
client int not null,
type varchar(02) not null,
date_start date not null,
date_expiration date not null,
date_status varchar(02),
card_security_code int not null,
foreign key (client) references clients(id_client)
);

create table investments(
id_investment int not null auto_increment primary key,
type varchar(02) not null,
amount numeric (12,2) not null,
investment_days int,
profit numeric(12,2),
interest_rate float,
start_date date not null,
expiration_date date,
account_source int not null,
foreign key (account_source) references accounts(id_account)
);

create table transactions(
id_transaction int not null auto_increment primary key,
type varchar(02) not null,
source int not null,  
account_destination int not null,
date_trx date not null,
transaction_amount numeric(12,2) not null,
debit_credit boolean not null,
client int not null,
foreign key (source) references accounts(id_account),
foreign key (account_destination) references accounts(id_account),
foreign key (source) references clients(id_client)
);
